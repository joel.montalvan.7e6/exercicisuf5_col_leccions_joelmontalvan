import java.util.Scanner

fun main(){
    val scanner = Scanner(System.`in`)
    val frutasAcumuladas = mutableSetOf<String>()

    while (true){
        println("Introduce todas las frutas que puedas sin repetir:")
        val frutasIntroducidas = scanner.next().uppercase()
        if (frutasIntroducidas == "END"){
            break
        }
        else if (frutasAcumuladas.contains(frutasIntroducidas)){
            println("MEEEC!!! YA HAS INTRODUCIDO ESTA FRUTA")
        }
        else {
            frutasAcumuladas.add(frutasIntroducidas)
        }
    }
}