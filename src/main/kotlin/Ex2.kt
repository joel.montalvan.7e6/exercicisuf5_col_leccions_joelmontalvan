import java.util.*

data class Trabajador (
    val dni: String,
    val nombre: String,
    val apellido: String,
    val direccion: String
)

fun main(){

    val trabajadores = mutableMapOf<String,Trabajador>()

    val scanner = Scanner(System.`in`)
    println("Introduce una cantidad de empleados:")
    val empleadosTotales = scanner.nextInt()

    for (i in 1 .. empleadosTotales){
        println("Introduce el DNI (EMPLEADO -> $i):")
        val dniEmpleado = scanner.next().uppercase()
        println("Introduce el nombre  (EMPLEADO -> $i):")
        val nombreEmpleado = scanner.next()
        println("Introduce el apellido  (EMPLEADO -> $i):")
        val apellidoEmpleado = scanner.next()
        println("Introduce una dirección  (EMPLEADO -> $i):")
        val direccionEmpleado = scanner.next()

        val datosDelEmpleado = Trabajador(dniEmpleado,nombreEmpleado,apellidoEmpleado,direccionEmpleado)
        trabajadores[dniEmpleado] = datosDelEmpleado

    }

    println("Introduce el DNI de un trabajador para obtener sus datos personales:")
    while (true){
        val dniTrabajador = scanner.next().uppercase()

        if (dniTrabajador != "END"){
            val datosDelTrabajador = trabajadores.getValue(dniTrabajador)
            println("EMPLEADO: ${datosDelTrabajador.nombre}, ${datosDelTrabajador.apellido} - ${datosDelTrabajador.dni}, ${datosDelTrabajador.direccion}")

        }
    }

}

