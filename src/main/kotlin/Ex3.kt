import java.util.Scanner
import kotlin.concurrent.thread

fun main(){
    val scanner = Scanner(System.`in`)
    val candidatosDelegados = mutableMapOf<String,Int>()

    while (true){
        println("Introduce a tu opción como proximo delegado de clase:")
        val votoDelegado = scanner.next().uppercase()
        if (votoDelegado == "END"){
            break
        }
        else if (votoDelegado !in candidatosDelegados) candidatosDelegados[votoDelegado] = 1
        else {
            candidatosDelegados[votoDelegado] = candidatosDelegados[votoDelegado]!! + 1
        }
    }
    println("Y el delegado de clase es ...")
    Thread.sleep(1000L)
    println(candidatosDelegados)
}