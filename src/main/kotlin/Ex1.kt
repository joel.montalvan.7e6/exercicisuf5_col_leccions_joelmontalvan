import java.util.*

fun main(){

    val carteles = mutableMapOf<Int,String>()

    val scanner = Scanner(System.`in`)
    println("Introduce una cantidad de carteles:")
    val cartelesTotales = scanner.nextInt()

    for (i in 1 .. cartelesTotales){
        println("Introduce un metro (CARTEL -> $i):")
        val metroCartel = scanner.nextInt()
        println("Introduce un texto  (CARTEL -> $i):")
        val textoCartel = scanner.next()
        carteles[metroCartel] = textoCartel
    }

    println("Cuantas consultas quieres hacer:")
    val consultasCarteles = scanner.nextInt()

    for (x in 1 .. consultasCarteles){
        println("Introduce un metro para localizar el cartel")
        val metroConsulta = scanner.nextInt()

        if (carteles.containsKey(metroConsulta)){
            println(carteles.getValue(metroConsulta))
        }
        else println("No existe este cartel")
    }

}

